# Relazione

Questa repository contiene la relazione del corso di _Informatica Forese_ dell'A.A. 2020/21 tenuto dal Prof. Cosimo Anglano.

## I soggetti della relazione

Vista la natura dell'elaborato, ovvero report tecnico con percorso pedagogico, il soggetto dovrebbe essere in 
terza persona. Per rimanere neutrale sia nel genere sia nel numero si è usato il soggetto indefinito _si_.

<blockquote cite="https://accademiadellacrusca.it/it/consulenza/usi-e-funzioni-del-pronome-clitico-si/173">
    <p>
        Il <i>si</i> &#39;impersonale&#39; &egrave; cos&igrave; definito perch&eacute;, usando la particella <i>si</i> 
        con valore di soggetto indefinito, &egrave; possibile fare la costruzione impersonale di qualsiasi verbo 
        intransitivo, oppure transitivo attivo (senza oggetto espresso) o passivo: <i>si va?</i>; <i>si ritiene opportuno
        questo provvedimento</i>.
    </p>
    <p>
        Nei costrutti impersonali con tempi composti, il participio passato &egrave; sempre volto al maschile se il verbo 
        impiegato ha nella costruzione personale l&#39;ausiliare <i>avere</i>: <i>si &egrave; detto troppo</i> (usato 
        personalmente, sarebbe infatti &#39;avete/abbiamo detto troppo&#39;); si declina invece al plurale, nella forma 
        maschile o femminile, se il verbo ha come ausiliare <i>essere</i>: <i>si &egrave; proprio caduti/e in basso</i> 
        (usato personalmente sarebbe &#39;siamo proprio caduti/e in basso&#39;).
    </p>
</blockquote>